package az.ingress.ms3.exception;

public class StudentAlreadyEixstException extends InvalidStateException {

    public StudentAlreadyEixstException(String email) {
        super("This email " + email + " is already taken");
    }
}
