package az.ingress.ms3.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CalculatorTest {


    private Calculator calculator = new Calculator();

    //a+b=c
    //given<condition>When<action>Then<ExpectedResult>
    @Test
    void givenAAndBWhenAddThenC() {
        //Arrange
        int a = 5;
        int b = 6;
        int c = 11;

        //Act
        int result = calculator.add(a, b);

        //Assert
        assertEquals(c, result);
    }

    //a-b=c
    @Test
    void givenAAndBWhenSubtractThenC() {
        //Arrange
        int a = 20;
        int b = 10;
        int c = 10;

        //Act
        int result = calculator.subtract(a, b);

        //Assert
        assertEquals(c, result);
    }


    @Test
    void givenAAndBWhenDivideThenC() {
        //Arrange
        int a = 20;
        int b = 10;
        int c = 2;

        //Act
        int result = calculator.divide(a, b);

        //Assert
        // assertEquals(c, result);
        assertThat(c).isEqualTo(result);
    }

    @Test
    void givenAAndBAndBIs0WhenDivideThenException() {
        //Arrange
        int a = 20;
        int b = 0;
        int c = 2;

        //Act & Assert
        assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(ArithmeticException.class)
                .hasMessage("/ by zero");
    }


}