package az.ingress.ms3.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "game")
public class GameStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "xcoordinate")
    private int xCoordinate;

    @Column(name = "ycoordinate")
    private int yCoordinate;

    private char player;

}
