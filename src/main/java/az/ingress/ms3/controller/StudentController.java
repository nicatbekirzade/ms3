package az.ingress.ms3.controller;

import az.ingress.ms3.dto.StudentDto;
import az.ingress.ms3.service.StudentService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable("id") Long id) {
        return studentService.findById(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody @Valid StudentDto studentDto) {
        return studentService.createStudent(studentDto);
    }


}
