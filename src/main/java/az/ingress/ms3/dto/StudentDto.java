package az.ingress.ms3.dto;

import com.sun.istack.NotNull;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class StudentDto {

    private Long id;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String name;

    @NotNull
    @NotEmpty(message = "can't be empty")
    @Size(min = 3, max = 15)
    private String lastName;

    @Positive
    private int age;

    @NotEmpty
    private String phone;

    @Email
    private String email;
}
