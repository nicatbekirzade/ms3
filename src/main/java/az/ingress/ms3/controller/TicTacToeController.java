package az.ingress.ms3.controller;

import az.ingress.ms3.dto.GameMoveDto;
import az.ingress.ms3.dto.GameStepDto;
import az.ingress.ms3.service.TicTacToeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("game")
public class TicTacToeController {

    private final TicTacToeService service;

    public TicTacToeController(TicTacToeService service) {
        this.service = service;
    }

    @GetMapping
    public List<GameStepDto> getBoard() {
        return service.getBoard();
    }

    @PutMapping
    public String play(@RequestBody GameMoveDto moveDto) {
        return service.play(moveDto);
    }

}
