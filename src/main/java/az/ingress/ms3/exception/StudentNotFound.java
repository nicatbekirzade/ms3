package az.ingress.ms3.exception;

public class StudentNotFound extends NotFoundException {

    private static final String MESSAGE = "No student with given found on the system";

    public StudentNotFound() {
        super(MESSAGE);
    }
}
