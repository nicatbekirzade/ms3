package az.ingress.ms3.service;

import az.ingress.ms3.dto.GameMoveDto;
import az.ingress.ms3.dto.GameStepDto;
import az.ingress.ms3.model.GameStep;
import az.ingress.ms3.repository.GameStepRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Service
@RequiredArgsConstructor
public class TicTacToeService {

    private final GameStepRepository gameStepRepository;
    private final ModelMapper modelMapper;
    private char nextPlayer = 'a';
    private char[][] board = new char[][]
            {
                    {'-', '-', '-'},
                    {'-', '-', '-'},
                    {'-', '-', '-'}
            };

    //x y  player
    public String play(GameMoveDto moveDto) {

        return play(moveDto.getXCoordiante(), moveDto.getYCoordinate());
    }

    public String play(int x, int y) {
        String result;
        printBoard();
        checkIfOutsideOfTheBoard(x, y);
        board[x][y] = nextPlayer;
        result = checkWinner();
        saveMove(x, y, nextPlayer);
        nextPlayer = updateLastPlayer();
        return result;
    }

    public List<GameStepDto> getBoard() {
        return gameStepRepository.findAll().stream()
                .map(i -> modelMapper.map(i, GameStepDto.class))
                .collect(Collectors.toList());
    }

    public char[][] getBoardStateAsArray(){
        final List<GameStep> allSteps = gameStepRepository.findAll();
        verifySteps(allSteps);
        allSteps.stream().forEach(step -> board[step.getXCoordinate()][step.getYCoordinate()]=step.getPlayer());
        return board;
    }

    private void verifySteps(List<GameStep> allSteps){

        allSteps.forEach(s ->{
            if(s.getYCoordinate()<0 || s.getXCoordinate()<0){
                throw new RuntimeException("Invalid Game Step");
            }
        });
    }

    private void saveMove(int x, int y, char nextPlayer) {
        GameStep gameStep = new GameStep();
        gameStep.setXCoordinate(x);
        gameStep.setYCoordinate(y);
        gameStep.setPlayer(nextPlayer);
        gameStepRepository.save(gameStep);
    }

    private void checkIfOccupied(int x, int y) {
        if (board[x][y] != '-') {
            throw new IllegalArgumentException("Board is already occupied");
        }
    }

    private char updateLastPlayer() {
        return nextPlayer == 'a' ? 'b' : 'a';
    }

    private String checkWinner() {
        return isWinner() ? nextPlayer + " is winner" :
                isDraw() ? "Result is draw" : "No winner";
    }

    private boolean isWinner() {
        int vertical = 0;
        int horizontal = 0;
        int diagonal1 = 0;
        int diagonal2 = 0;
        for (int i = 0; i < board.length; i++) {
            vertical = vertical + board[0][i];
            horizontal = horizontal + board[i][0];
            diagonal1 = diagonal1 + board[board.length - 1 - i][i];
            diagonal2 = diagonal2 + board[i][i];
        }
        return isWinner(horizontal) || isWinner(vertical) || isWinner(diagonal1) || isWinner(diagonal2);
    }

    private boolean isWinner(int sum) {
        return sum == (nextPlayer * 3);
    }

    private void validateUserInput(int x, int y) {
        checkIfOutsideOfTheBoard(x, y);
        checkIfOccupied(x, y);
    }

    private void checkIfOutsideOfTheBoard(int xCoordinate, int yCoordinate) {
        checkIfCoordinatesAreOnBoard(xCoordinate, 'x');
        checkIfCoordinatesAreOnBoard(yCoordinate, 'y');
    }

    private void checkIfCoordinatesAreOnBoard(int coordinate, char c) {
        if (coordinate < 0 || coordinate > 2) {
            throw new IllegalArgumentException(c + " is outside of the board");
        }
    }

    private void printBoard() {
        Arrays.stream(board).forEach(item -> System.out.println(item));
    }

    private boolean isDraw() {
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] == '\0') {
                    return false;
                }
            }
        }
        return true;
    }



}
