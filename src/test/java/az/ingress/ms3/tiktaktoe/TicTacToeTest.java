package az.ingress.ms3.tiktaktoe;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import az.ingress.ms3.dto.GameStepDto;
import az.ingress.ms3.model.GameStep;
import az.ingress.ms3.repository.GameStepRepository;
import az.ingress.ms3.service.TicTacToeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TicTacToeTest {


    @Mock
    private GameStepRepository gameStepRepository;

    @Spy
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<GameStep> gameStepArgumentCaptor;
    private TicTacToeService game;

    @BeforeEach
    void setUp() {
        game = new TicTacToeService(gameStepRepository, modelMapper);
    }

    @Test
    void givenFirstTurnWhenNextPlayerThenX(){
        //Act & Assert
        assertThat(game.getNextPlayer()).isEqualTo('X');


    }
    void givenLastTurnWasXWhenNextPleyerThenO(){
        //Arrange
        int x = 2;
        int y = 2;
//        GameStep gameStep = new GameStep();
//        gameStep.setxCoordinate(x);
//        gameStep.setyCoordinate(y);
//        gameStep.setPlayer('X');

        //Act
        game.play(x,y);

        //Assert
        assertThat(game.getNextPlayer()).isEqualTo('O');
//        verify(gameStepRepository, times(1).save(gameStep));
        verify(gameStepRepository, times(1)).save(gameStepArgumentCaptor.capture());
        final GameStep value = gameStepArgumentCaptor.getValue();
        assertThat(value.getXCoordinate()).isEqualTo(x);
        assertThat(value.getYCoordinate()).isEqualTo(y);
        assertThat(value.getPlayer()).isEqualTo('X');
    }
    @Test
    public void givenFirstMoveWhenPlayThenNextPlayerIsA() {
        //Act
        final char nextPlayer = game.getNextPlayer();

        //Assert
        assertThat(nextPlayer).isEqualTo('a');
    }

    @Test
    public void givenXIsNegativeExpectException() {
        //Arrange
        int x = -1;
        int y = 1;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("x is outside of the board");
    }

    @Test
    public void givenYIsNegativeExpectException() {
        //Arrange
        int x = 1;
        int y = -1;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("y is outside of the board");
    }

    @Test
    void givenXIsGreaterThanTwoThenException() {
        //Arrange
        int x = 3;
        int y = 1;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("X cannot be greater than 2");
    }


    @Test
    void givenYIsGreaterThanTwoThenException() {
        //Arrange
        int x = 0;
        int y = 3;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("y is outside of the board");
    }


    @Test
    void givenOccupiedThenException() {
        //Arrange
        int x = 0;
        int y = 0;

        //Act & Assert
        game.play(x, y);
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Board is already occupied");
    }


    @Test
    void givenHorizontalLineThenWin() {
        //Arrange
        game.play(0, 0); //A
        game.play(1, 2);//B
        game.play(0, 1);//A
        game.play(1, 1);//B

        //Act
        String result = game.play(0, 2);//A

        //Assert
        assertThat(result).isEqualTo("a is winner");
    }

    @Test
    void givenVerticalLineThenWin() {
        //Arrange
        game.play(0, 0); //A
        game.play(1, 2);//B
        game.play(1, 0);//A
        game.play(1, 1);//B

        //Act
        String result = game.play(2, 0);//A

        //Assert
        assertThat(result).isEqualTo("a is winner");
    }

    @Test
    void givenDiagonalLineThenWin() {
        //Arrange
        game.play(0, 0); //A
        game.play(1, 2);//B
        game.play(1, 1);//A
        game.play(1, 0);//B

        //Act
        String result = game.play(2, 2);//A

        //Assert
        assertThat(result).isEqualTo("a is winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        //Arrange
        game.play(1, 0); // X
        game.play(0, 0); // O
        game.play(2, 0); // X
        game.play(0, 1); // O
        game.play(1, 1); // X

        //Act
        String actual = game.play(0, 2); // O

        assertThat(actual).isEqualTo("b is winner");
    }

    void whenGetBoardThenBoard(){
        //Arrange
        GameStep gameStep = new GameStep();
        gameStep.setId(1l);
        gameStep.setPlayer('X');
        gameStep.setXCoordinate(1);
        gameStep.setYCoordinate(2);
        when(gameStepRepository.findAll()).thenReturn(List.of(gameStep));


        GameStepDto gameStepDto = new GameStepDto();
        gameStepDto.setId(1l);
        gameStepDto.setPlayer('X');
        gameStepDto.setXCoordinate(1);
        gameStepDto.setYCoordinate(2);

        //Act
        assertThat(game.getBoard()).isEqualTo(List.of(gameStepDto));
    }
    @Test
    void whenGetBoardThenExpectBoard(){
        //Arrange
//        when(gameStepRepository.findAll()).thenReturn(List.of(createGameStep(0,-1)));
        doReturn(List.of(createGameStep(0,0))).when(gameStepRepository).findAll();

        //Act
        char[][] board = game.getBoardStateAsArray();

        //Assert
        assertThat(board).isEqualTo(createExpectedBoardState());
    }

    @Test
    void whenGetBoardThenExpectExceptionInvalidState(){
        //Arrange
        doReturn(List.of(createGameStep(0,-1))).when(gameStepRepository).findAll();

        //Act

//        char[][] board = game.getBoardStateAsArray();

        //Assert
        assertThatThrownBy(() ->game.getBoardStateAsArray())
                .isInstanceOf((RuntimeException.class))
                .hasMessage("Invalid Game Step");
    }

    private GameStep createGameStep(int x, int y){
        GameStep gameStep = new GameStep();
        gameStep.setXCoordinate(x);
        gameStep.setYCoordinate(y);
        gameStep.setPlayer('a');

        return gameStep;
    }

    private char[][] createExpectedBoardState(){
        return new char[][]
                {
                        {'a', '-', '-'},
                        {'-', '-', '-'},
                        {'-', '-', '-'}
                };
    }

}
