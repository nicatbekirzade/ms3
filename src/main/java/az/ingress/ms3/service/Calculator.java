package az.ingress.ms3.service;

public class Calculator {

    public int add(int a, int b) {
        return addAandB(a, b);
    }

    private int addAandB(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    // jacoco
    // mutation coverage, pitest
}
