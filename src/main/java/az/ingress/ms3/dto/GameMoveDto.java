package az.ingress.ms3.dto;

import lombok.Data;

@Data
public class GameMoveDto {

    private int xCoordiante;
    private int yCoordinate;

}
