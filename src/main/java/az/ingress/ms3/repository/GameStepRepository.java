package az.ingress.ms3.repository;

import az.ingress.ms3.model.GameStep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameStepRepository extends JpaRepository<GameStep, Long> {
}
