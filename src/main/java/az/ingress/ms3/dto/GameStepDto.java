package az.ingress.ms3.dto;

import lombok.Data;

@Data
public class GameStepDto {

    private Long id;
    private int xCoordinate;
    private int yCoordinate;
    private char player;

}
